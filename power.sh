#!/bin/bash
set -x
set -e

power() {
	/usr/local/bin/sigrok-cli \
		--driver=korad-kaxxxxp:conn=/dev/serial/by-id/usb-Nuvoton_USB_Virtual_COM_0036B681024E-if00 \
		--config "enabled=${1}" \
		--set
}

case $1 in
"on" | "off")
	power $1
	;;
reset)
	power off
	sleep 2
	power on
esac

