# Daimler 8295 Lava Conf

Lava configuration for Daimler Civic LS4plus


# Prerequisite

- Use a modified abl that force fastboot mode on each boot.

```
$ git clone https://gitlab-apertispro.boschdevcloud.com/mju6kor/dai_civic_abl.git -b lava
$ cd dai_civic_abl
$ bash build.sh
$ fastboot flash abl out/makena/abl/abl.elf
```

- Gen 4.2 debug board
- Use usb device for flashing

# Power supply

For this setup we use a TENMA 72-2710 programmable power supply connected to the dispatcher through usb.

The power.sh need an upstream git sigrok version in order to support this power supply.

# Lava configuration

## device type
file: lava-server/dispatcher-config/device-types/daimler-civic-ls4-plus.jinja2
Device type: daimler-civic-ls4plus

A simple fastboot device type.
Boot partition is not flashed but directly loaded into ram on boot.

## device instance
file: lava-server/dispatcher-config/devices/daimler-civic-ls4-plus-01.jinja2

Specify the usbid and fastboot id.
(use fastboot devices, and lsusb both ID should be the same)

Set connection_command to match the serial adapter in use.

Prefer using serial by-id rather than index:

/dev/serial/by-id/usb-FTDI_Quad-G3G-RS232-DebugAdapter_FT5O2EIS-if01-port0

instead of

/dev/ttyUSB1

Since device numbering depends on how much usb to serial adapter are connected in.

The power supply commands are specific to the TENMA 72-2710.

Fix the path to where 'power.sh' is installed on the dispatcher.

```
{% set hard_reset_command = '/home/user/power.sh reset' %}
{% set power_on_command = '/home/user/power.sh on' %}
{% set power_off_command = '/home/user/power.sh off' %}
```
## Job example

Assuming the file are available through a web server

```
device_type: daimler-civic-ls4-plus
job_name: 8295 pipeline, first job
timeouts:
  job:
    minutes: 15
  action:
    minutes: 5
  connection:
    minutes: 2
priority: medium
visibility: public
actions:
- deploy:
    timeout:
      minutes: 5
    to: fastboot
    images:
      boot:
        url: http://192.168.1.24:8080/cts-hw-pack-ls4p/sa8295-boot.img
      dtbo:
        url: http://192.168.1.24:8080/cts-hw-pack-ls4p/sa8295-dtbo.img
      cts_system_c:
        url: http://192.168.1.24:8080/cts-hw-pack-ls4p/daimlercivic_v2021-target-ls4p-arm64-rfs-image-sparse-qti.ext4
- boot:
    prompts:
    - 'root@(.*):/#'
    timeout:
      minutes: 15
    method: fastboot
```

## Dispatcher dependencies

android-sdk-libsparse-utils: provides img2simg and simg2img which allow to
unsparse an image and applying a test overlay.

fastboot: for flashing and booting an image.

## Board and dispatcher setup

Lava dispatcher is connected to the lava-apertispro-dev lava server.

The dispatcher is also connected to the controller supply, the usb device for flashing
the target and to the Gen4 degug board to get the serial output.


```


                                     -----------------------
                               -----/                       \-----
                             -/                                   \-
                            ( lava-apertispro-dev.boschdevcloud.com )
                             -\                                   /-
                               -----\                       /-----
                                     ----------+------------
                                               |
                                               |
                                  +------------+-------------+
                                  |                          |
                                  |      Lava                +--
                                  +      Dispatcher          |  \----\    USB /dev/ttyACM0
                                  |    "julien-worker"       |       \-------\
                                  |                          |            \-----\
                                  +-----------+--------------+                 \----\
 +--------------------+       --/             |                               +-----\-----------+
 |                    |    --/  Serial        |                               |                 |
 |   Debug gen4       |  -/   Output          |                               |  Tenma 72-2710  |
 |                    +-/                     |                               |  Bench Power    |
 |                    |                       |                               |    Supply       |
 |                    |                       | USB Fastboot flashing         |                 |
 +--------------------+                       |                               +---------/-------+
         \-                                   |                                       -/
          \--                                 |                                     -/
            \-                                |                                    /
             \--                              |                                  -/
               \-                             |                                -/         12V
                 \-      +--------------------+-------------------------+     /
                   \--   |                                              |   -/
                      \- |                                              | -/
                        \+                 SA8295p                      |/
                         |          Daimler Civic LS4 plus              |
                         |                                              |
                         |                                              |
                         +----------------------------------------------+
```
